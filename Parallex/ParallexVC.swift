//
//  ParallexVC.swift
//  Parallex
//
//  Created by Appiskey's iOS Dev on 31/12/2019.
//  Copyright © 2019 Appiskey. All rights reserved.
//

import UIKit

class ParallexVC: UIViewController {

//    @IBOutlet weak var parallexView: ParallexView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupParallexView()
    }
    
    func setupParallexView(){
        let leftBarItem = ParallexNavBarItem.init(image: UIImage.init(named: "back")!) {
            print("back")
        }
        let rightBarItem = ParallexNavBarItem.init(image: UIImage.init(named: "camera1")!) {
            print("camera")
        }
        let navConfig = ParallexNavConfig.init(leftBarItem: leftBarItem,
                                               rightBarItem: rightBarItem,
                                               title: "Nav Title",
                                               itemsColor: UIColor.white,
                                               barColor: UIColor.black)
        
        
        let image1 = ImageCellData.init(imageURL: URL.init(string: "https://robbreportedit.files.wordpress.com/2019/08/svj6301.jpg?w=1000")!,
                                        imageInstance: nil)
        
        let image2 = ImageCellData.init(imageURL: URL.init(string: "https://lamborghiniclubamerica.com/app/uploads/2017/09/Lamborghini-GIRO-2017.jpg")!,
                                        imageInstance: nil)
        
        let image3 = ImageCellData.init(imageURL: URL.init(string: "https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/640x400/quality/80/https://s.aolcdn.com/commerce/autodata/images/O3LGGIC1.jpg")!,
                                        imageInstance: nil)
        
        let image4 = ImageCellData.init(imageURL: URL.init(string: "https://www.lamborghini.com/sites/it-en/files/DAM/lamborghini/model/huracan/Evo/restyle/3_RP---Huracan-Evo-88.jpg")!,
                                        imageInstance: nil)
        let image5 = ImageCellData.init(imageURL: nil, imageInstance: UIImage.init(named: "img"))
        
        let imagesData = [image1,
                          image2,
                          image3,
                          image4,
                          image5]
        
        let parallexConfig = ParallexConfiguration.init(navConfig: navConfig,
                                                        viewController: self,
                                                        parallexTVDelegate: self,
                                                        parallexTVDataSource: self,
                                                        headerSlider: imagesData,
                                                        pagerSelectedColor: UIColor.yellow.withAlphaComponent(0.8),
                                                        pagerUnSelectedColor: UIColor.white.withAlphaComponent(0.8))

        ParallexViewInitializer.init(config: parallexConfig, parallexView: self.view)
    }

}

extension ParallexVC: ParallexTableViewDataSource{
    func parallexTableViewNumberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func parallexTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func parallexTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor(red: .random(in: 0...1),
                                        green: .random(in: 0...1),
                                        blue: .random(in: 0...1),
                                        alpha: 1.0)
        cell.selectionStyle = .none
        return cell
    }
    
    func parallexTableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0 * CGFloat(indexPath.row + 1)
    }
}

extension ParallexVC : ParallexTableViewDelegate{
    func parallexTableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}
