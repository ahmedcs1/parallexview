//
//  ViewController.swift
//  Parallex
//
//  Created by Appiskey's iOS Dev on 10/12/2019.
//  Copyright © 2019 Appiskey. All rights reserved.
//

import UIKit
class ViewController: UIViewController {

    @IBOutlet weak var heightOfImgView: NSLayoutConstraint!
    @IBOutlet weak var topImgView: UIImageView!
    @IBOutlet weak var tableView: MyTableView!
    @IBOutlet weak var parallexHeader: UIView!
    
    // variable to save the last position visited, default to zero
    private var lastContentOffset: CGFloat = 0
    private var initialScrollInsect: CGFloat = 0
    private var contentHeight: CGFloat = 0
    
    private var thresholdScrollHeightForHeader : CGFloat = 0.0
    var alpha : CGFloat = 0.0
    private var headerHeight : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.parallexHeader.backgroundColor = UIColor.black.withAlphaComponent(0.0)

        self.initialScrollInsect = self.tableView.contentOffset.y
        (self.tableView.superview as? UIScrollView)?.delegate = self
        
        tableView.reloadDataCompletionBlock = {
            print("reload Data")
        }
        tableView.contentHeightUpdated = { (height) in
            self.contentHeight = height//self.tableView.contentSize.height// ?? 0.0
            self.thresholdScrollHeightForHeader = (height * 0.35)
            if self.thresholdScrollHeightForHeader < self.tableView.frame.height{
                self.thresholdScrollHeightForHeader = (height * 0.35) - self.tableView.frame.height
            }
            
            print("content Height: ", self.contentHeight)
        }
    }

}

extension ViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor(red: .random(in: 0...1),
                                        green: .random(in: 0...1),
                                        blue: .random(in: 0...1),
                                        alpha: 1.0)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0 * CGFloat(indexPath.row + 1)
    }
}

extension ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}


extension ViewController: UIScrollViewDelegate{
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        print("scrool to top")
        return true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrolling")

        if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height),
                                        animated: false)
        }
        if (scrollView.contentOffset.y < self.initialScrollInsect && self.heightOfImgView.constant < 400) {
            self.heightOfImgView.constant = self.heightOfImgView.constant - scrollView.contentOffset.y
            self.view.layoutIfNeeded()
        }else if (scrollView.contentOffset.y > self.initialScrollInsect && self.heightOfImgView.constant > 200) {
            self.heightOfImgView.constant = self.heightOfImgView.constant - scrollView.contentOffset.y
            self.view.layoutIfNeeded()
        }
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            print("move up")
            if !scrollView.isAtTop && !scrollView.isAtBottom{
                if self.alpha > 0{
                    self.alpha = (scrollView.contentOffset.y - (self.parallexHeader.alpha * self.thresholdScrollHeightForHeader)) / self.thresholdScrollHeightForHeader
                    self.parallexHeader.backgroundColor = UIColor.black.withAlphaComponent(alpha)
                }
                if scrollView.contentOffset.y <= self.thresholdScrollHeightForHeader{
                    let valueToMutliple = self.initialScrollInsect / scrollView.contentOffset.y
                    let calculatedHeight = (valueToMutliple) * 200//self.heightOfImgView.constant
                    if calculatedHeight > 64{
                        self.heightOfImgView.constant = calculatedHeight
                        self.view.layoutIfNeeded()
                    }
                }
            }else if scrollView.isAtBottom{
                scrollView.contentOffset = .zero
            }
        }
        
        else if (self.lastContentOffset < scrollView.contentOffset.y) {
            print("move down")
            if !scrollView.isAtTop && !scrollView.isAtBottom{
                if self.alpha < 1{
                    self.alpha = (scrollView.contentOffset.y) / self.thresholdScrollHeightForHeader
                    self.parallexHeader.backgroundColor = UIColor.black.withAlphaComponent(alpha)
                    if self.heightOfImgView.constant > 64{
                        if (1.0 - self.alpha) * self.heightOfImgView.constant < 64{
                            self.heightOfImgView.constant = 64
                        }else{
                            self.heightOfImgView.constant = (1.0 - self.alpha) * self.heightOfImgView.constant
                        }
                        self.view.layoutIfNeeded()
                            
                    }
                }
            }else if scrollView.isAtTop{
                scrollView.contentOffset = .zero
            }
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("end")
        self.endScrolling()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate{
            self.endScrolling()
        }
    }
    
    func endScrolling(){
        if self.heightOfImgView.constant > 200{
            self.heightOfImgView.constant = 200
            UIView.animate(withDuration: 0.6) {
                self.view.layoutIfNeeded()
            }
        }
    }
}
